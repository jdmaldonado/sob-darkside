const express = require('express');

const prospectService = require('./services/prospect.service');

let app = express();

app.set('port', 8800);

app.listen(app.get('port'), function () {
	console.log('Express server listening on port ' + app.get('port'));
	
	prospectService.evaluateProspects();
	// prospectService.setDefaultStatus();

});