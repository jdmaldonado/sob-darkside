'use strict';

var admin = require("firebase-admin");

var serviceAccount = require('./../config/sob-admin-service-account.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://sob-admin.firebaseio.com"
});

module.exports = {
  firebaseDb: admin.database()
};