const {
  firebaseDb
} = require('./firebase.service');
const _ = require('lodash');
const asyncLoop = require('node-async-loop');
const collection = 'prospects';
const userService = require('./user.service');

const _convertToArray = function _convertToArray(prospects) {

  let arrayProspects = [];

  _.forEach(prospects, (prospect, key) => {
    prospect.id = key;
    arrayProspects.push(prospect);
  });

  return arrayProspects;
}

const _setStatus = function _setStatus(prospect) {
  return new Promise((resolve, reject) => {

    if (!prospect) reject({
      message: `Prospect doesn't exist`
    })

    let newStatus;

    if (prospect.consultant) {
      newStatus = 'ASSIGNED'
    } else {
      newStatus = 'UNASSIGNED'
    }

    let ref = firebaseDb.ref(`${collection}/${prospect.id}`);
    ref.update({
      status: newStatus
    }, (fbError) => {
      if (!fbError) {
        resolve();
      } else {
        reject(fbError);
      }
    });
  });
}

const _setUserInfo = function _setUserInfo(prospect, user) {
  return new Promise((resolve, reject) => {

    if (!prospect) reject('prospect invalid!');
    if (!user) reject('prospect user!');

    let ref = firebaseDb.ref(`${collection}/${prospect.id}`);
    ref.update({
      status: user.status,
      userAppInfo: user
    }, (fbError) => {
      if (!fbError) {
        resolve();
      } else {
        reject(fbError);
      }
    });
  });
}


const getAllProspects = function getAllProspects() {
  return new Promise((resolve) => {
    let ref = firebaseDb.ref(`${collection}`)
    ref.once('value', (prospects) => {
      let arrayProspects = _convertToArray(prospects.val());
      resolve(arrayProspects);
    });
  });
}

const getProspectsByStatus = function getProspectsByStatus(status) {
  return new Promise((resolve) => {
    let ref = firebaseDb.ref(`${collection}`).orderByChild('status').equalTo(status);
    ref.once('value',
      (prospects) => {
        let arrayProspects = _convertToArray(prospects.val());
        resolve(arrayProspects);
      },
      (err) => reject(err)
    );
  });
}

const evaluateProspects = function evaluateProspects() {
  Promise.all([userService.getAllUsers(), getAllProspects()])
    .then((data) => {
      let users = data[0];
      let prospects = data[1];

      asyncLoop(prospects,
        (prospect, next) => {

          let user = _.filter(users, {
            'email': prospect.email
          });

          if (user.length > 0) {
            console.log(`trying... ${prospect.id}`);

            _setUserInfo(prospect, user[0])
              .then(() => next())
              .catch(() => {
                console.log(`Error updating prospect ${prospect.id}`);
                next();
              });
          } else {
            next();
          }

        },
        (err) => {
          if (err) console.log('err', err);
          console.log('Finishhh !!!!')
        }
      );

    })
    .catch((err) => console.log(err));
}

const setDefaultStatus = function setDefaultStatus() {
  getAllProspects()
    .then((prospects) => {
      asyncLoop(prospects,
        (prospect, next) => {

          if (!prospect.status)  {

            if (prospect.consultant) {
              console.log(prospect);
              console.log('----------------------');
              next();

              // _setStatus(prospect)
              //   .then(() => next())
              //   .catch((err) => {
              //     console.log(err.message);
              //     next();
              //   })
            } else {
              next();
            }
          } else {
            next();
          }
        },
        (err) => {
          if (err) {
            console.log('err', err);
          } else {
            console.log('Finish');
          }
        }
      )
    })
    .catch((err) => console.log(err.message));
}


module.exports = {
  getAllProspects,
  setDefaultStatus,
  evaluateProspects
}