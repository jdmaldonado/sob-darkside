const {
  db
} = require('./mysql.service');

const _getArray = function _getArray(objectItems) {

  if (typeof objectItems === 'object') {

    let keys = Object.keys(objectItems);
    keys.forEach(item => {
      array.push(item);
    });

    return array;
  }
}

const getAllUsers = function getAllUsers() {

  return new Promise((resolve, reject) => {
    db.connect((err) => {
      if (err) reject(err);

      const query = `
    SELECT
        id, 
        email,
        first_name AS firstName,
        last_name AS lastName,
        DATE(created) AS createdDate,
        Vencimiento AS expirationDate,
        DATEDIFF(Vencimiento, DATE(created)  ) AS freeDays,
        DATEDIFF(Vencimiento, DATE(CURDATE()) ) AS expirationDays,
        CASE
          WHEN DATEDIFF(Vencimiento, DATE(CURDATE())) > 31 THEN 'PREMIUM'
          WHEN DATEDIFF(Vencimiento, DATE(CURDATE())) < 0 THEN 'EXPIRED'
          ELSE 'FREE'
        END AS status
      FROM users
      WHERE email NOT IN ('maldo.s@hotmail.com', 'niko@aloja.in', 'admin@example.com', 'lidiagom2006@gmail.com', 'jodagomz6@gmail.com', 'lgarcia@socialbusiness.com.co', 'gerencia@socialbusiness.com.co')
      ORDER BY expirationDays DESC;
    `;

      db.query(query, (err, result) => {
        if (err) reject(err);

        resolve(result);

      });

    })
  })
};

module.exports = {
  getAllUsers
}